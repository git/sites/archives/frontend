PER_PAGE = 100

def index_name(list)
  'ml-' + list
end

def threads_in_month(list, year, month, page)
  $es.search(
    index: index_name(list),
    size: PER_PAGE,
    from: PER_PAGE * (page - 1),
    body: {
      query: {
        filtered: {
          filter: {
            and:
            [
              { term:    { month: to_monthint(year, month) } },
              { missing: { field: 'parent' } }
            ]
          }
        }
      },
      filter: { not: { filter: { term: { hidden: true } } } },
      sort: {
        date: 'desc'
      }
    }
  )
end

def most_recent(list, n)
  result = $es.search(
    index: index_name(list),
    size: n,
    body: {
      sort: { date: 'desc' },
      filter: { not: { filter: { term: { hidden: true } } } }
    }
  )

  if result['hits']['total'] == 0
    return []
  else
    return result['hits']['hits']
  end
rescue => _e
  []
end

def messages_in_month(list, year, month, page)
  $es.search(
    index: index_name(list),
    size: PER_PAGE,
    from: PER_PAGE * (page - 1),
    body: {
      query: {
        filtered: {
          filter: {
            term: { month: to_monthint(year, month) }
          }
        }
      },
      filter: { not: { filter: { term: { hidden: true } } } },
      sort: {
        date: 'desc'
      }
    }
  )
end

def get_month_listing(list)
  $es.search(
    index: index_name(list),
    search_type: 'count',
    size: 0,
    body: {
      size: 0,
      filter: { not: { filter: { term: { hidden: true } } } },
      aggs: {
        messages_by_month: {
          terms: {
            field: 'month',
            size: 0,
            order: {
              month2: 'desc'
            }
          },
          aggs: {
            month2: {
              avg: {
                field: 'month'
              }
            }
          }
        }
      }
    }
  )
end

def get_message_count_internal(list)
  $es.search(
    index: index_name(list),
    size: 0,
    body: {
        filter: { not: { filter: { term: { hidden: true } } } }
    }
  )['hits']['total']
rescue => _e
  '?'
end

def get_message(list, hash)
  $es.search(
    index: index_name(list),
    size: 1,
    body: {
      query: { filtered: { filter: { term: { _id: hash } } } },
      filter: { not: { filter: { term: { hidden: true } } } }
    }
  )
end

def get_parent_data(list, parent_id)
  return nil if parent_id.nil?

  parent = $es.search(
    index: index_name(list),
    body: {
        query: { match: { _id: parent_id } },
        filter: { not: { filter: { term: { hidden: true } } } }
    }
  )

  if parent['hits']['total'] == 0
    return nil
  else
    return parent['hits']['hits'].first
  end
end

def get_child_data(list, parent_id)
  return nil if parent_id.nil?

  children = $es.search(
    index: index_name(list),
    size: 100,
    body: {
      query: { match: { parent: parent_id } },
      filter: { not: { filter: { term: { hidden: true } } } },
      sort: { date: 'asc' }
    }
  )

  if children['hits']['total'] == 0
    return nil
  else
    return children['hits']['hits']
  end
end
