require 'date'

# Caches the message count for each email
# I guess this operation is/was expensive at some point.
class MessageCountCache
  include Singleton
  CACHE_SECONDS = 3600

  def initialize
    update!
  end

  def update!
    @message_counts ||= {}

    @new_counts = {}
    [$config['active_lists'], $config['frozen_lists']].flatten.each do |list|
      @new_counts[list] = get_message_count_internal(list)
    end

    @message_counts = @new_counts
    @load_date = DateTime.now
  end

  def [](list)
    update?

    @message_counts[list]
  end

  private

  def update?
    update! if ((DateTime.now - @load_date) * 60 * 60 * 24).to_i > CACHE_SECONDS
  end
end

# Caches the most recent messages displayed on the front page
class MostRecentMessagesCache
  include Singleton
  CACHE_SECONDS = 3600

  def initialize
    update!
  end

  def update!
    @messages ||= {}

    @new_messages = {}
    [$config['active_lists'], $config['frozen_lists']].flatten.each do |list|
      @new_messages[list] = most_recent(list, 5)
    end

    @messages = @new_messages
    @load_date = DateTime.now
  end

  def [](list)
    update?

    @messages[list] || []
  end

  private

  def update?
    update! if ((DateTime.now - @load_date) * 60 * 60 * 24).to_i > CACHE_SECONDS
  end
end
