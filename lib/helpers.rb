require 'rinku'
require 'erb'

MARC_FIND = 'https://marc.info/?i='.freeze
GOOGLEGROUP_SEARCH = 'https://groups.google.com/forum/#!search/messageid$3A'.freeze

helpers do
  def list_check
    unless [$config['active_lists'], $config['frozen_lists']].flatten.include?(params[:list])
      status 404
      body 'List not found'
      return false
    end

    true
  end

  def monthint_to_link(monthint)
    date = DateTime.parse('%s-%s-01' % [monthint[0..3], monthint[4..5]])

    '<a href="threads/%s/">%s</a>' % [date.strftime('%Y-%m'), date.strftime('%Y %B')]
  end

  def date_format(date)
    DateTime.iso8601(date).strftime('%a, %d %b %Y %T')
  end

  def to_monthint(year, month)
    ('%i%02i' % [year.to_i, month.to_i]).to_i
  end

  def to_month(year, month)
    date = DateTime.parse('%s-%s-01' % [year, month])
    date.strftime('%B %Y')
  end

  def h(text)
    Rack::Utils.escape_html(text)
  end

  def u(text)
    ERB::Util.url_encode(text)
  end

  # This method strips domains from header fields.
  def strip_email_headers(ary)
    [ary].flatten.map do |email|
      email.gsub(/@(\S*?)('|"|>|$)/) do
        if $1 == 'gentoo.org'
          "@g.o#{$2}"
        elsif $1 == 'lists.gentoo.org'
          "@l.g.o#{$2}"
        else
          domain, _dot, tld = $1.rpartition '.'
          "@#{'×' * domain.length}.#{tld}#{$2}"
        end
      end
    end
  end

  # This method is meant to strip emails from free text, not header fields
  def strip_email(str)
    str.gsub(/([a-zA-Z0-9._%+-]+)@([a-zA-Z0-9.-]+)\.([a-zA-Z]{2,10})/) do
      if $2 == 'gentoo' && $3 == 'org'
        "#{$1}@g.o"
      elsif $2 == 'lists.gentoo' && $3 == 'org'
        "#{$1}@l.g.o"
      else
        "#{$1}@#{'×' * $2.length}.#{$3}"
      end
    end
  end

  def linkize(str)
    Rinku.auto_link(str, :urls, 'rel="nofollow"')
  end

  def msgid_to_marc(msgid)
    # We have to transform the msg-id first
    # CAK86ViRgefgrb0qUyjQdYa+6C5BTiNvn8UKZwQsMvcJmY-L0mg@mail.gmail.com
    # CAK86ViRgefgrb0qUyjQdYa+6C5BTiNvn8UKZwQsMvcJmY-L0mg () mail ! gmail ! com
    # http://marc.info/?i=CAK86ViRgefgrb0qUyjQdYa+6C5BTiNvn8UKZwQsMvcJmY-L0mg%20()%20mail%20!%20gmail%20!%20com
    # pan.2009.08.28.00.00.38@cox.net
    # http://marc.info/?i=pan.2009.08.28.00.00.38%20()%20cox%20!%20net
    local, host = msgid.split('@', 2)
    new_msgid = local + ' () ' + host.gsub('.', ' ! ')
    MARC_FIND + ERB::Util.url_encode(new_msgid)
  end

  def msgid_to_googlegroup(msgid)
    # We have to transform the msg-id first
    # "m2nf4d$9uu$1@dont-email.me"
    # search is 'messageid:"m2nf4d$9uu$1@dont-email.me"'
    # https://groups.google.com/forum/#!search/messageid$3A%22m2nf4d$249uu$241@dont-email.me%22
    GOOGLEGROUP_SEARCH + ERB::Util.url_encode('"' + msgid + '"')
  end
end
