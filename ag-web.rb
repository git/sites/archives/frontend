#!/usr/bin/env ruby
# archives.gentoo.org: web frontend
#
# Copyright 2015 Alex Legler <a3li@gentoo.org>
# Licensed under the terms of the AGPLv3.

require 'bundler/setup'
require 'yaml'
require 'sinatra'
require 'sinatra/partial'
require 'elasticsearch'
require 'date'
require 'pony'

require_relative 'lib/index.rb'
require_relative 'lib/helpers.rb'
require_relative 'lib/cache.rb'

configure do
  set :partial_template_engine, :erb
  mime_type :atom, 'application/atom+xml'
end

$es = Elasticsearch::Client.new(log: false)
$es.transport.reload_connections!

$config = YAML.load_file('config.yml')

MessageCountCache.instance.update!
MostRecentMessagesCache.instance.update!

get '/:list/report/:msgid' do
  return unless list_check

  begin
    result = get_message(params[:list], params[:msgid])

    if result['hits']['total'] == 0
      status 404
      body 'Message not found.'
      return
    end

    result_data = result['hits']['hits'].first
    @title = 'Report %s - %s' % [h(result_data['_source']['subject']), params[:list]]

    erb :report, locals: { message: result_data, list: params[:list] }
  rescue Exception => e
    $stderr.puts e.to_s
    status 500
  end
end

post '/report' do
  return unless list_check

  begin
    result = get_message(params[:list], params[:msgid])

    if result['hits']['total'] == 0
      status 404
      body 'Message not found.'
      return
    end

    result_data = result['hits']['hits'].first
    @title = 'Report %s - %s' % [h(result_data['_source']['subject']), params[:list]]

    msg = ''
    if params[:captcha] == $config['report_captcha']
      Pony.mail(
        to: $config['report_addr'],
        from: 'archives.gentoo.org <postmaster@gentoo.org>',
        subject: "Reported Message on #{params[:list]}: #{result_data['_source']['subject']}",
        body: erb(:reportmail, locals: { message: result_data, list: params[:list] }, layout: false)
      )
      msg = 'Thanks for your report.'
    else
      msg = 'Invalid CAPTCHA. Report not sent.'
    end

    erb :reportsent, locals: { message: result_data, list: params[:list], msg: msg }
  rescue Exception => e
    $stderr.puts e.to_s
    status 500
  end
end

get '/:list/' do
  return unless list_check

  begin
    result = get_month_listing(params[:list])
    @title = params[:list]
    current_monthint = to_monthint(Date.today.year, Date.today.month)

    erb :listindex, locals: { results: result, list: params[:list], current_monthint: current_monthint }
  rescue => e
    $stderr.puts e.to_s
    status 500
  end
end

get '/:list/threads/?' do
  redirect '/%s/' % [params[:list]], 301
end

get '/:list/messages/?' do
  redirect '/%s/' % [params[:list]], 301
end

get '/:list/threads/:year-:month' do
  redirect '/%s/threads/%s-%s/' % [params[:list], params[:year], params[:month]], 301
end

get '/:list/threads/:year-:month/:page/' do
  redirect '/%s/threads/%s-%s/%s' % [params[:list], params[:year], params[:month], params[:page]], 301
end

get '/:list/threads/:year-:month/:page?' do
  return unless list_check

  begin
    @title = params[:list]
    current_page = [(params[:page] || 1).to_i, 1].max
    result = threads_in_month(params[:list], params[:year], params[:month], current_page)
    no_threads = false

    if result['hits']['total'] == 0
      result = messages_in_month(params[:list], params[:year], params[:month], current_page)
      no_threads = true
    end
    max_pages = (result['hits']['total'].to_f / PER_PAGE).ceil

    erb :listmonth, locals: {
      results: result,
      list: params[:list],
      current_page: current_page,
      max_pages: max_pages,
      mode: :threads,
      no_threads: no_threads
    }
  rescue => e
    $stderr.puts e.to_s
    status 500
  end
end

get '/:list/messages/:year-:month' do
  redirect '/%s/messages/%s-%s/' % [params[:list], params[:year], params[:month]], 301
end

get '/:list/messages/:year-:month/:page/' do
  redirect '/%s/messages/%s-%s/%s' % [params[:list], params[:year], params[:month], params[:page]], 301
end

get '/:list/messages/:year-:month/:page?' do
  return unless list_check

  begin
    @title = params[:list]
    current_page = [(params[:page] || 1).to_i, 1].max
    result = messages_in_month(params[:list], params[:year], params[:month], current_page)
    max_pages = (result['hits']['total'].to_f / PER_PAGE).ceil

    erb :listmonth, locals: {
      results: result,
      list: params[:list],
      current_page: current_page,
      max_pages: max_pages,
      mode: :messages,
      no_threads: false
    }
  rescue => e
    $stderr.puts e.to_s
    status 500
  end
end

get '/:list/message/:msgid/' do
  redirect '/%s/message/%s' % [params[:list], params[:msgid]], 301
end

get '/:list/message/:msgid' do
  return unless list_check

  begin
    result = get_message(params[:list], params[:msgid])

    if result['hits']['total'] == 0
      status 404
      body 'Message not found.'
      return
    end

    result_data = result['hits']['hits'].first
    @title = '%s - %s' % [h(result_data['_source']['subject']), params[:list]]
    parent_data = get_parent_data(params[:list], result_data['_source']['parent'])
    child_data = get_child_data(params[:list], params[:msgid])

    erb :message, locals: { message: result_data, list: params[:list], parent: parent_data, children: child_data }
  rescue Exception => e
    $stderr.puts e.to_s
    status 500
  end
end

get '/lists' do
  @nav = :lists
  erb :lists
end

get '/' do
  @nav = :index
  erb :index
end
