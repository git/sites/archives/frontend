$(function() {
  var table = $('.ag-header-table');

  if (table === undefined) {
    return;
  }

  if ($('.ag-quote').size() === 0) {
    return;
  }

  var btn = $("<button class=\"btn btn-sm btn-outline-secondary d-none d-sm-block ag-toggle-quotes\"><span class=\"fa fa-quote-left\"></span> Toggle quotes</button>");
  btn.insertAfter(table);
  btn.click(function() {
    $('.ag-quote').each(function(index) {
      $(this).toggleClass('ag-quote-hidden');
    });
  });

  $('.ag-quote').each(function(index) {
    // Don't hide quotes by default. Maybe put in some cookie stuff to remember the choice
    // $(this).addClass('ag-quote-hidden');

    $(this).click(function() {
      $('.ag-quote').each(function(index) {
        $(this).toggleClass('ag-quote-hidden');
      });
    });
  });
});
